import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

/**
 * Created by yieh on 5/22/17.
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestManageTasks {

    static AndroidConfigClass d = new AndroidConfigClass();
    WebDriverWait wait = new WebDriverWait(d.getDriver(), 20);

    @BeforeClass
    public static void beforeClass() {

    }

    @Test
    public void testAAccessTheTaskView() throws Exception {
        wait.until(visibilityOfElementLocated(By.id("org.tasks:id/fab")));
        d.getDriver().findElement(By.id("org.tasks:id/fab")).click();
    }

    @Test
    public void testBAddANewTask() throws Exception {
        wait.until(visibilityOfElementLocated(By.id("org.tasks:id/title")));
        d.getDriver().findElement(By.id("org.tasks:id/title")).sendKeys("Bring Milk");
        d.getDriver().findElement(By.id("org.tasks:id/priority_high")).click();
        d.getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageButton")).click();
    }

    @Test
    public void testCAddASecondTask() throws Exception {
        d.getDriver().findElement(By.id("org.tasks:id/fab")).click();
        d.getDriver().findElement(By.id("org.tasks:id/title")).sendKeys("Bring Eggs");
        d.getDriver().findElement(By.id("org.tasks:id/priority_medium")).click();
        d.getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageButton")).click();
    }

    @Test
    public void testDSearchForATask() throws Exception {
        wait.until(visibilityOfElementLocated(By.id("org.tasks:id/rowBody")));
        d.getDriver().findElementById("Search").click();
        d.getDriver().findElement(By.id("org.tasks:id/search_src_text")).sendKeys("Bring Milk"+"\n");
    }

    @Test
    public void testEDeleteTheFirstTask() throws Exception {
        wait.until(visibilityOfElementLocated(By.id("org.tasks:id/rowBody")));
        d.getDriver().findElementById("org.tasks:id/title").click();
        d.getDriver().findElementById("Delete task").click();
        d.getDriver().findElementById("android:id/button1").click();
    }

    @AfterClass
    public static void tearDown() {
        d.getDriver().quit();
    }
}
