/**
 * Created by yieh on 5/22/17.
 */

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class AndroidConfigClass {
    File appDir = new File("./apk/");
    File app = new File(appDir, "org.tasks_448.apk");

    static AndroidDriver driver;
    DesiredCapabilities capabilities = new DesiredCapabilities();

    public AndroidConfigClass() {
        capabilities.setCapability("deviceName", "emulator-5554");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("fullReset", true);
        capabilities.setCapability("clearSystemFiles", true);
        capabilities.setCapability("automationName", "appium");

        URL url = null;

        try {
            url = new URL("http://0.0.0.0:4723/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver = new AndroidDriver(url, capabilities);
        System.out.printf(driver.toString());

    }

    public AndroidDriver getDriver() {
        return driver;
    }
}
