# README #

This README would normally document whatever steps are necessary to get your application up and running.

## How do I get set up? ##

### Required Installations ###
- Install InteliJ Community or any equivalent IDE
- Install NodeJS
- Install JDK
- Install Maven
- Install brew (Mac)
- Install Android SDK
### Configuration ###
- Open the terminal
- Install appium by running the command npm install appium
- Install appium-doctor by running the command npm install appium-doctor
- install carthage by running the command brew install carthage
- Create the environment variables for JAVA, MAVEN, ANDROID
- Create an Android emulator
- Clone this repository

### How to run tests ###
- mvn test